import { Pie } from 'react-chartjs-2';
import {Alert} from 'react-bootstrap'

export default function IncomeAndExpensePieChart({totalIncome,totalExpense}){

	const types = ["Income","Expenses"]

	const data = {
			labels: types, 
			datasets: [
				{
					data:[totalIncome,totalExpense],
					backgroundColor:["gold","silver"],
					hoverBackgroundColor:["aliceblue","green"]
				}
			]
		}
		return(
			<>
			<h1 className="incomeExpensePie">Income & Expenses</h1>
			{
				(totalIncome > 0 || totalExpense > 0)
				?
				<>
					<Pie data={data}></Pie>
					<p className="paragraph"> The chart illustrates your total income and total expenses. If the income is larger, there is a savings otherwise, you are having a deficit</p>
				</>
				:
				<Alert variant='warning'>
					No data to display. You have to record  transactions.
				</Alert>
			}
			</>
			
			)
	}




