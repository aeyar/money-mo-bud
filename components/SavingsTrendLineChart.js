import { Line } from 'react-chartjs-2';
import {Alert} from 'react-bootstrap'


export default function SavingsTrendLineChart({savingsTrend,allTransactions}){

	const labels = ['January','February','March','April','May','June','July','August','September','October','November','December']

	const data = {
		labels: labels,
		datasets: [
		{
			data: savingsTrend,
			label: 'Savings',
			backgroundColor: 'transparent',
			borderWidth: 7,
			borderColor: 'gold',
			pointRadius: 5,
			pointBorderColor: 'silver',
			spanGaps: true
		},
		{
			data: [1,1,1,1,1,1,1,1,1,1,1,1],
			label: 'Baseline',
			borderColor: 'red',
			pointRadius: 2,
		}
		]
	}


	return(
		<>
			<h1 className="savingsTrendLabel">Savings Trend</h1>
			{
				allTransactions.length > 0 
				?
				<>
					<Line data={data}/>
					<p className="paragraph"> The chart shows the savings trend from the percentage of your monthly expenses over your monthly income. As interpretation, the straight horizontal line shows the consistency of your expenses based on your income. If the lines goes up, you are able to save more than previous month without increasing your expenditure. But if lines goes down, your expenditure increased but your income does not proportionately. Lastly, if the line hits below the red line, it means you expenditure is greater than you what you earn.</p>
				</>
				:
				<Alert variant='warning'>
					No data to display. You have to record transactions.
				</Alert>
			}

		</>
	)
}