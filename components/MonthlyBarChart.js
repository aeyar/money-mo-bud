import {Bar} from 'react-chartjs-2';
import {Tabs, Tab, Alert} from 'react-bootstrap';

export default function MonthlyBarChart({monthlyExpense,monthlyIncome,allTransactions}){

	
	const incomeDataSets = {
				label: "Income",
				backgroundColor: "gold",
				borderColor: "silver",
				hoverBackgroundColor: "green",
				data: monthlyIncome
			}

	const expenseDatasets = {
				label: "Expenses",
				backgroundColor: "silver",
				borderColor: "gold",
				hoverBackgroundColor: "blue",
				data: monthlyExpense
			}

	const labels = ['January','February','March','April','May','June','July','August','September','October','November','December']



	return(
		<>
			<h1 className="monthlyBarChart">Monthly Income and Expenses</h1>
			<Tabs defaultActiveKey="incomeAndExpense" id="barInsight">
				<Tab eventKey="incomeAndExpense" title="Both">
				{
					allTransactions.length > 0 
					?
					<Bar data={{
								labels:labels,
								datasets:[
									incomeDataSets,
									expenseDatasets	
								]}}
								/>
					:
					<Alert variant='warning'>
						No data to display. You have to record transactions.
					</Alert>
				}
					</Tab>
				<Tab eventKey="income" title="Income">
				{
					allTransactions.length > 0 
					?
					<Bar data={{
								labels:labels,
								datasets:[
									incomeDataSets
								]}}
								/>
					:
					<Alert variant='warning'>
						No data to display. You have to record transactions.
					</Alert>
				}
				</Tab>
				<Tab eventKey="expense" title="Expense">
				{
					allTransactions.length > 0 
					?
					<Bar data={{
								labels:labels,
								datasets:[
									expenseDatasets
								]}}
								/>
					:
					<Alert variant='warning'>
						No data to display. You have to record transactions.
					</Alert>
				}
				</Tab>
			</Tabs>
			{
				allTransactions.length > 0 ?
				<p className="paragraph">
				The chart shows the movement of your income and expenses monthly. You can see on which month got the highest or lowest on income and expenses. You can switch the tabs if you want to see its movement individually.</p> : ""
			}
		</>
		)
}