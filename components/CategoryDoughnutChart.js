import {Doughnut} from 'react-chartjs-2';
import {Alert} from 'react-bootstrap'

export default function CategoryDoughnutChart({expenseCategories,incomeCategories,totalExpenseByCategories,totalIncomeByCategories,expenseColorData,incomeColorData,allTransactions}){

	const expenseData = {
		labels : expenseCategories,
		datasets: [
		{
			label: 'Expenses By Category',
			data: totalExpenseByCategories,
			backgroundColor: expenseColorData,
			hoverBackgroundColor: expenseColorData
		}
		]
	}

	const incomeData = {
		labels : incomeCategories,
		datasets: [
		{
			label: 'Income By Category',
			data: totalIncomeByCategories,
			backgroundColor: expenseColorData,
			hoverBackgroundColor: expenseColorData
		}
		]
	}
	


	return(
	<div>
		<h1 className="savingsTrendLabel">Expenses Chart</h1>
		{
			allTransactions.length > 0 
			?
			<>
				<Doughnut data={expenseData}/>
				<p className="paragraph"> The chart shows the distribution of your expenses as per its categories. You can see on where you spend the most.</p>
			</>
			:
			<Alert variant='warning'>
				No data to display. You have to record transactions.
			</Alert>
			
		}
		<h1 className="savingsTrendLabel">Income Chart</h1>
		{
			allTransactions.length > 0 
			?
			<>
				<Doughnut data={incomeData}/>
				<p className="paragraph"> The chart shows the distribution of your income as per its categories. You can see the main source of your income.</p>
			</>
			:
			<Alert variant='warning'>
				No data to display. You have to record transactions.
			</Alert>
		}
	</div>
	)
}