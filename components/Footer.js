export default function Footer() {
    return (
        <footer className="bg-light" style={footerstyle}>
        App developed by ALLAN ROBIN L. BUENO || 2020
       </footer>
    )
}

const footerstyle ={
    marginTop:'100px',
    padding:'1em',
    width:'100%',
    textAlign:'right',
    left: 0,
    bottom:0
}
