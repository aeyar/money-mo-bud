import { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from './../UserContext';
import Image from 'next/image';


export default function NavBar(){

	const {user} = useContext(UserContext)

	return (
		<Navbar className="navBarHead" style={{backgroundColor: 'beige'}} expand="lg" >
            {
                user.id !== null 
                ?
                <Link href="/dashboard" >
                    <a className="border border-0"> 
                        <Image src='/logo.png' width='100' height='40' alt='logo'/>
                    </a>
                </Link>
                :
                <Image src='/logo.png' width='100' height='40' alt='logo'/>        
            }
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        { 
                            user.id !== null ?
                            <>
                                <Link href="/dashboard">
                                    <a className="nav-link" role="button">Dashboard</a>
                                </Link>
                                <Link href="/insights">
                                    <a className="nav-link" role="button">Insights</a>
                                </Link>
                                <Link href="/transactions">
                                    <a className="nav-link" role="button">Transactions</a>
                                </Link>
                                <Link href="/logout">
                                    <a className="nav-link" role="button">Logout</a>
                                </Link>
                            </>
                            :
                            <>
                                <Link href="/login">
                                    <a className="nav-link" role="button">Login</a>
                                </Link>
                                <Link href="/register">
                                    <a className="nav-link" role="button">Register</a>
                                </Link>
                            </>
                        }
                    </Nav>
                </Navbar.Collapse>
        </Navbar>


		);

}