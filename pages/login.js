import Head from 'next/head';
import { useState, useContext } from 'react';
import { Form, Button, Card, Row, Col, Spinner } from 'react-bootstrap';
import UserContext from './../UserContext';
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router';
import Swal from 'sweetalert2';
import Image from 'next/image';


export default function login(){

    const {setUser} = useContext(UserContext)

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [tokenId, setTokenId] = useState(null)

    const [notLoadingButton, setNotLoadingButton] = useState(true)


    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, options)
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            Swal.fire({
                        icon:'success',
                        title:'Success!',
                        text: 'You have Logged In via Google Account',
                        footer: 'You will be redirected to your dashboard'
                    })
            setUser({ id: data._id})
            Router.push('/dashboard')
        })
    }

    const authenticateGoogleToken = (response) => {
        setTokenId(response.tokenId)
        setNotLoadingButton(false)
        const payload = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/verify-google-id-token`, payload)
        .then(res => res.json())
        .then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error')
                    setNotLoadingButton(true)
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                    setNotLoadingButton(true)
                }
            }
        })
    }


    function authenticate(e) {
        e.preventDefault();
        setNotLoadingButton(false)
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                email, 
                password
            })
        })
        .then( res=> res.json())
        .then( data => {
            // data
            if( data.accessToken) {
                localStorage['token'] = data.accessToken;

                fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
                    headers : {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then( res => res.json())
                .then( user =>{
                    setUser({
                        id: user._id
                    })
                    Swal.fire({
                        icon:'success',
                        title:'Success!',
                        text: 'You have Logged In via Email Account',
                        footer: 'You will be redirected to your dashboard'
                    })
                    Router.push('/dashboard')
                })
            } else {
                Swal.fire({
                        icon:'error',
                        title:'Incorrect Login Details'
                    })
                setNotLoadingButton(true)

                // Router.push('/error')
            }
        })
    }

    return (
        <>
            <Head>
                <title>Login || Money Mo</title>
            </Head>
                <div className='d-flex justify-content-center'>
                <Card className="card my-5 rounded-0 text-center bg-transparent border-0" sm={12} md={6}>
                   {/* <Card.Header>
                        <h1 className='font-weight-bold'>
                            Login
                        </h1>
                    </Card.Header>*/}
                    <Row className='d-flex flex-item-center'>
                        <Col className='border-0 margin-0 padding-0' sm={10} md={6}>
                            <Image src='/login.png' width='268' height='389' alt='login' />
                        </Col>
                        <Col className='border-0 margin-0 padding-0'>
                            <Card.Body className="text-left">
                                <Form onSubmit={e=>authenticate(e)}>
                                    <Form.Group controlId='userEmail'>
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control
                                            className="rounded-0"
                                            type="email"
                                            value={email}
                                            onChange={ e => setEmail(e.target.value)}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Group controlId='password'>
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control
                                            className="rounded-0"
                                            type="password"
                                            value={password}
                                            onChange={ e => setPassword(e.target.value)}
                                            required
                                        />
                                    </Form.Group>
                                    {
                                        notLoadingButton 
                                        ?
                                        <>
                                        <Button className="mb-2" variant='dark' type="submit" className="rounded-0" block >Login</Button>
                                        <Card.Text className='text-center text-muted' >or you may</Card.Text>
                                        <GoogleLogin

                                                clientId="251677720537-sjg7v04idrbqk42nojt95jorkb49gqb3.apps.googleusercontent.com"
                                                buttonText="Login with Your Google Account"
                                                onSuccess={ authenticateGoogleToken }
                                                onFailure={ authenticateGoogleToken }
                                                cookiePolicy={ 'none' }
                                                className="border w-100 text-center d-flex justify-content-center "
                                            />
                                        </>
                                        :
                                        <Button block disabled variant='transparent'>
                                            <Spinner size="sm" animation="grow" variant="dark" />
                                            <Spinner size="sm" animation="grow" variant="dark" />
                                            <Spinner size="sm" animation="grow" variant="dark" />
                                             {" "}Please wait....
                                        </Button>
                                    }
                                </Form>
                            </Card.Body>
                        </Col>
                    </Row>
                </Card>
            </div>
        </>
    );
}

