import { Jumbotron, Row, Col, ListGroup, Tabs, Tab, Form, Button, Container, DropdownButton, Alert, Dropdown} from 'react-bootstrap';
import {useEffect, useContext, useState} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Moment from 'moment'
import { faAngleRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Image from 'next/image';


export default function transactions(){

	const {user} = useContext(UserContext)

	const [allTransactions,setAllTransactions] = useState([])
	const [expenseTransactions, setExpenseTransactions] = useState([])
	const [incomeTransactions, setIncomeTransactions] = useState([])
	const [expenseCategories,setExpenseCategories] = useState([])
	const [incomeCategories,setIncomeCategories] = useState([])
	const [category, setCategory] = useState('')
	const [sortedTransactions, setSortedTransactions] = useState([])

	const [searchTarget, setSearchTarget] = useState ('')
	const [searchResult, setSearchResult] = useState ([])
	const [searchType, setSearchType] = useState ('simple')

	const [key, setKey] = useState('home');

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers:{
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json())
		.then(data => {
			setAllTransactions(data.transactions)
			setSortedTransactions(data.transactions.sort((currEl,nextEl) => {
					// console.log(currEl,nextEl)
					if (currEl.date < nextEl.date){
						return 1
					} else if (currEl.date > nextEl.date){
						return -1
					} else {
						return 0
					}
				}));
			let tempExpenseTransactions = []
			let tempIncomeTransactions = []
			let tempExpenseCategories = []
			let tempIncomeCategories = []
			data.transactions.forEach(transaction=>{
				if(transaction.type === 'expense'){
					tempExpenseTransactions.push(transaction)}

				if(transaction.type === 'income'){
					tempIncomeTransactions.push(transaction)}

				if(transaction.type === 'expense'){
					if(!tempExpenseCategories.find(category => category === transaction.category)){
						tempExpenseCategories.push(transaction.category) 
					}

				}
				
				if(transaction.type === 'income'){
					if(!tempIncomeCategories.find(category => category === transaction.category)){
						tempIncomeCategories.push(transaction.category)
					}
				}


			})
			setExpenseCategories(tempExpenseCategories)
			setIncomeCategories(tempIncomeCategories)
			setExpenseTransactions(tempExpenseTransactions)
			setIncomeTransactions(tempIncomeTransactions)

		})
	},[user.id])


	const search = e => {
  		e.preventDefault()		
  		if (searchTarget !== "") {
  			if(searchType === 'simple'){
  				const matchResult = allTransactions.filter(transaction =>	
	  			(
	  				transaction.category.includes(searchTarget.toLowerCase()) || 
	  				transaction.type.includes(searchTarget.toLowerCase()) || 
	  				transaction.description.includes(searchTarget.toLowerCase()) || 
	  				(transaction.amount).toString() == searchTarget)
	  			)
	  			if(matchResult.length>0){
	  				Swal.fire({
	                    icon:'info',
	                    title:`${matchResult.length} record/s has been found`
	                })
	  			} else {
	  				Swal.fire({
	                    icon:'error',
	                    title:'no record found'
	                })
	  			}
	  			setSearchResult(matchResult)
	  		} else if (searchType === 'category'){
	  			const matchResult = allTransactions.filter(transaction =>	
	  			( 
	  				transaction.category === searchTarget.toLowerCase()
	  				)
	  			)
	  			if(matchResult.length>0){
	  				Swal.fire({
	                    icon:'info',
	                    title:`${matchResult.length} record/s has been found`
	                })
	                setShowCategoryButton(true)
	                setShowDateButton(true)
	                setShowGoButton(false)
	                setShowSearchButton(true)
	                setShowSearchBar(true)
	                setSearchType('simple')
	  			} else {
	  				Swal.fire({
	                    icon:'error',
	                    title:'no record found'
	                })
	  			}
	  			setSearchResult(matchResult)
	  		} else if (searchType === 'date'){
	  			if(startDate !== endDate){
	  				const matchResult = allTransactions.filter(transaction =>	
	  				( 
	  					(transaction.date >= startDate && transaction.date <= endDate )
		  				)
		  			)
		  			if(matchResult.length>0){
		  				Swal.fire({
		                    icon:'info',
		                    title:`${matchResult.length} record/s has been found`
		                })
		                setShowCategoryButton(true)
		                setShowDateButton(true)
		                setShowGoButton(false)
		                setShow2Dates(false)
		                setStartDate("")
		                setEndDate("")
		                setShowSearchButton(true)
		                setShowSearchBar(true)
		                setSearchType('simple')
		  			} else {
		  				Swal.fire({
		                    icon:'error',
		                    title:'no record found'
		                })
		  			}
		  			setSearchResult(matchResult)
	  			} else {
	  				Swal.fire({
		                    icon:'error',
		                    title:'Start Date and End Date Must Not Be The Same'
		                })
	  			}
	  			
	  		}	
  		} else {
  			Swal.fire({
                    icon:'error',
                    title:'Cannot be blank'
                })
  			setSearchResult([])
  		}
  		setSearchTarget('')
  		setShowSearchResultTab(true)
  		setKey('searchResult')
  	}

  	const [showSearchButton,setShowSearchButton] = useState(true)
  	const [showSearchResultTab,setShowSearchResultTab] = useState(false)
  	const [showGoButton,setShowGoButton] = useState(false)
  	const [showCategoryButton,setShowCategoryButton] = useState(true)
  	const [show2Buttons,setShow2Buttons] = useState(false)
  	const [showDateButton, setShowDateButton] = useState(true)
  	const [show2Dates, setShow2Dates] = useState(false)
  	const [showSearchBar,setShowSearchBar] = useState(true)
  	const [startDate,setStartDate] = useState('')
  	const [endDate,setEndDate] = useState('')
  	const [showDateSubmit, setShowDateSubmit] = useState(false)
	
  	const tester = "border border-dark"

	return(
		<>
			<Row className="border-bottom border-dark">
				<Col className="d-flex align-items-center" sm={12}>
					<h1>TRANSACTION RECORDS</h1>
				</Col>
				<Col >
					<Image src='/search.png' width='300' height='300' alt='search'/>
				</Col>

			</Row>
			<Form onSubmit={(e)=> search(e)}>
		        <Form.Group>
	          		{
	          			showSearchBar ? 
		          		<Form.Control
				            className="rounded-0"
				            type='text'
				            placeholder='Enter here'
				            onChange={e => setSearchTarget(e.target.value)}
				            value={searchTarget}
				          /> : ""
	          		}
	          		<Row>
	          			<Col>
	          				{
			        			showSearchButton ?
			        			<Button className="rounded-0" variant='info' type='submit'>Search</Button> : ""
			        		}
			        		{ 
			        			showSearchResultTab ?
			        			<Button className="rounded-0" variant='danger' onClick={()=>{setShowSearchResultTab(false),setKey('home'),setShowCategoryButton(true),setShowGoButton(false),setShowSearchButton(true),setShow2Buttons(false),setShowDateButton(true),setShow2Dates(false),setStartDate(""),setEndDate(""),setShowSearchBar(true),setSearchTarget(''),setSearchType('simple')}}>Close Search Result</Button> : ""

			        		}
	        			</Col>
	        			

	        			<Col className='text-right'>
	        			{
	        				show2Buttons ?
	        				<div className="btn-group">
				        		<DropdownButton className="rounded-0" variant='dark' style={{backgroundColor: 'black'}} title="Income" alignRight onClick={()=>setSearchType('category')}>
										{	
											incomeCategories.map((category, i) =>{
											return(
												<Dropdown.Item 
												className="rounded-0"
												key={i} 
												onClick={()=> {setCategory(category),
															setSearchTarget(category),
															setShowGoButton(true),
															setShow2Buttons(false)
															}}
															>
												{category}
												</Dropdown.Item>
											)
										})
									}
								</DropdownButton>
								<DropdownButton className="rounded-0" variant='dark' style={{backgroundColor: 'black'}} title="Expenses" alignRight onClick={()=>setSearchType('category')}>
										{	
											expenseCategories.map((category, i) =>{
											return(
												<Dropdown.Item 
												className="rounded-0"
												key={i} 
												onClick={()=> {setCategory(category),
															setSearchTarget(category),
															setShowGoButton(true),
															setShow2Buttons(false)
															}}
															>
												{category}
												</Dropdown.Item>
											)
										})
									}
								</DropdownButton>
	        				</div>	: ""
	        			}
								{
									showGoButton ?
									<Button type='submit' className="rounded-0" onSubmit={(e)=>{setShowGoButton(false),setShowCategoryButton(true),search(e)}}>SHOW ALL {category.toUpperCase()}</Button> : ""
								}
	        			{
	        				showCategoryButton ?
			        		<Button variant='warning' className="rounded-0" onClick={()=>{setShow2Buttons(true),setShowCategoryButton(false),setShowSearchButton(false),setShowDateButton(false),setShowSearchBar(false)}}>Category</Button> : ""
	        			}
	        			{
	        				showDateButton ?
			        		<Button variant='dark' className="rounded-0" onClick={()=>{setShow2Dates(true),setShowCategoryButton(false),setShowSearchButton(false),setShowDateButton(false),setShowSearchBar(false),setShowSearchResultTab(false),setKey('home')}}>Date</Button> : ""
	        			}
	        			{
	        				show2Dates ?
	        				<>
	        					<Row>
	        						Start Date
		        					<Form.Control
		        						type='date'
		        						value={startDate}
		        						onChange={(e)=>{setStartDate(e.target.value),setSearchType('date'),setSearchTarget('date')}}
		        					/>
	        					</Row>
	        					<Row>
	        						End Date
		        					<Form.Control
		        						type='date'
		        						value={endDate}
		        						onChange={(e)=>{setEndDate(e.target.value),setSearchType('date'),setSearchTarget('date')}}
	        					/>
	        					</Row>
	        					
	        				</> : ""
	        			}
	        			{
	        				(startDate !== "" && endDate !== "") ?
		        			<Button className="rounded-0" variant='dark' type='submit'>
		        				Submit Date
		        			</Button> : ""
	        			}
			        	</Col>

	          		</Row>
		          
		
		        </Form.Group>
		     </Form>
			

			<ListGroup variant='flush'>
				<Tabs id="transactionsPage" activeKey={key} onSelect={(k) => setKey(k)}>
					<Tab eventKey="home" title="All Transactions">
						{
							sortedTransactions.map(item => (
								<ListGroup.Item key={item._id} className='text-right'>
									<Row>
										<Col className='mr-auto text-left' sm={6}>
											{
												item.type === 'income' 
												?
												<span className='text-success'>
													{Moment(item.date).format('MM/DD/YY')} <FontAwesomeIcon icon={faAngleRight} /> {item.category.toUpperCase()} <FontAwesomeIcon icon={faAngleRight} /> ‟ {item.description} ” 
												</span>	
												:
												<span className='text-danger'>
													{Moment(item.date).format('MM/DD/YY')} <FontAwesomeIcon icon={faAngleRight} /> {item.category.toUpperCase()} <FontAwesomeIcon icon={faAngleRight} /> ‟ {item.description} ” 
												</span>	
											}
										</Col>
										<Col className='ml-auto' sm={6}>
											{
												item.type === 'income' ?
											<span className='text-success font-weight-bold'>
												Php {item.amount.toFixed(2)}
											</span>	 :
											<span className='text-danger font-weight-bold'>
												Php {item.amount.toFixed(2)}
											</span>
											}	
										</Col>
									</Row>
									 
								</ListGroup.Item>
								))
						}
					</Tab>
					<Tab className="tabsTransaction "eventKey="allIncome" title="Income">
						{
							incomeTransactions.map(item => (
								<ListGroup.Item key={item._id} className='text-right'>
									<Row>
										<Col className='mr-auto text-left' sm={6}>
											{
												item.type === 'income' 
												?
												<span className='text-success'>
													{Moment(item.date).format('MM/DD/YY')} <FontAwesomeIcon icon={faAngleRight} /> {item.category.toUpperCase()} <FontAwesomeIcon icon={faAngleRight} /> ‟ {item.description} ” 
												</span>	
												:
												<span className='text-danger'>
													{Moment(item.date).format('MM/DD/YY')} <FontAwesomeIcon icon={faAngleRight} /> {item.category.toUpperCase()} <FontAwesomeIcon icon={faAngleRight} /> ‟ {item.description} ” 
												</span>	
											}
										</Col>
										<Col className='ml-auto' sm={6}>
											{
												item.type === 'income' ?
											<span className='text-success font-weight-bold'>
												Php {item.amount.toFixed(2)}
											</span>	 :
											<span className='text-danger font-weight-bold'>
												Php {item.amount.toFixed(2)}
											</span>
											}		
										</Col>
									</Row>
									 
								</ListGroup.Item>
								))
						}
						
					</Tab>
					<Tab eventKey="allExpenses" title="Expenses">
					{
							expenseTransactions.map(item => (
								<ListGroup.Item key={item._id} className='text-right'>
									<Row>
										<Col className='mr-auto text-left' sm={6}>
											{
												item.type === 'income' 
												?
												<span className='text-success'>
													{Moment(item.date).format('MM/DD/YY')} <FontAwesomeIcon icon={faAngleRight} /> {item.category.toUpperCase()} <FontAwesomeIcon icon={faAngleRight} /> ‟ {item.description} ” 
												</span>	
												:
												<span className='text-danger'>
													{Moment(item.date).format('MM/DD/YY')} <FontAwesomeIcon icon={faAngleRight} /> {item.category.toUpperCase()} <FontAwesomeIcon icon={faAngleRight} /> ‟ {item.description} ” 
												</span>	
											}
										</Col>
										<Col className='ml-auto' sm={6}>
											{
												item.type === 'income' ?
											<span className='text-success font-weight-bold'>
												Php {item.amount.toFixed(2)}
											</span>	 :
											<span className='text-danger font-weight-bold'>
												Php {item.amount.toFixed(2)}
											</span>
											}		
										</Col>
									</Row>
									 
								</ListGroup.Item>
								))
						}
						
					</Tab>
				{
					showSearchResultTab === true
					?
					<Tab eventKey="searchResult" title="Search Result" unmountOnExit>
					{
					searchResult.length > 0 ?

					searchResult.map(item => (
							
							<ListGroup.Item key={item._id} className='text-right'>
								<Row>
									<Col className='mr-auto text-left' sm={6}>
										{
												item.type === 'income' 
												?
												<span className='text-success'>
													{Moment(item.date).format('MM/DD/YY')} <FontAwesomeIcon icon={faAngleRight} /> {item.category.toUpperCase()} <FontAwesomeIcon icon={faAngleRight} /> ‟ {item.description} ” 
												</span>	
												:
												<span className='text-danger'>
													{Moment(item.date).format('MM/DD/YY')} <FontAwesomeIcon icon={faAngleRight} /> {item.category.toUpperCase()} <FontAwesomeIcon icon={faAngleRight} /> ‟ {item.description} ” 
												</span>	
											}
										</Col>
										<Col className='ml-auto' sm={6}>
											{
												item.type === 'income' ?
											<span className='text-success font-weight-bold'>
												Php {item.amount.toFixed(2)}
											</span>	 :
											<span className='text-danger font-weight-bold'>
												Php {item.amount.toFixed(2)}
											</span>
											}		
									</Col>
								</Row>
								 
							</ListGroup.Item>
							)) : 
							<Alert variant='info'>No Results Found</Alert>
					}		
					</Tab>
					:
					""
				}
				</Tabs>
			</ListGroup>
		</>
	)
}