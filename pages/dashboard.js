import {Button, Container, Jumbotron, Modal, ListGroup, Row, Col, Form, DropdownButton, Dropdown} from 'react-bootstrap';
import React, { useState, useEffect, useContext } from 'react';
import Image from 'next/image';
import Swal from 'sweetalert2';
import UserContext from '../UserContext' ;
import Moment from 'moment';
import Router from 'next/router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChartLine, faScroll, faCashRegister, faCoins } from '@fortawesome/free-solid-svg-icons'
import Link from 'next/link';
import Head from 'next/head';


// import IncomeAndExpenseLineChart from '../components/IncomeAndExpenseLineChart'

export default function dashboard(){

	const { user,unsetUser } = useContext(UserContext);


	// details needed for recording of expenses
	const [amount, setAmount] = useState('')
	const [date, setDate] = useState('')
	const [description, setDescription] = useState('')
	const [category, setCategory] = useState('')
	const [type, setType] = useState('')
	const [customerName, setCustomerName] = useState('')

	const[recentUpdate,setRecentUpdate] = useState('')

	
	// store of current values
	const [categories,setCategories] = useState([])
	const [allTransactions,setAllTransactions] = useState([])
	const [totalIncome, setTotalIncome] = useState(0)
	const [totalExpense, setTotalExpense] = useState(0)
	const totalBalance = (totalIncome - totalExpense)

	// store of current values 2
	const [Updatedcategories,setUpdatedcategories] = useState([])
	

	
	
	// for rerendering of transactions
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers:{
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json())
		.then(data => {
			
			
		})
	},[user.id,category,amount,date])

	// for rendering of Categories, Total Income, Total Expense
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers:{
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.transactions !== undefined){data.transactions.forEach((transaction, index, array)=>{
				if(index === array.length - 1)
				setRecentUpdate(`An ${transaction.type.toUpperCase()} under ${transaction.category.toUpperCase()} category in the amount of Php ${transaction.amount.toFixed(2)} for ${Moment(transaction.date).format('Do of MMMM YYYY')}`)
			})}
			if(data.fName !== undefined){
				setCustomerName(data.fName)
			} else {
				setCustomerName('Please Login Again')
			}
			setAllTransactions(data.transactions)
			let tempcategories = []
			if(data.transactions !== undefined){data.transactions.forEach(transaction =>{
				if(transaction.type === type.toLowerCase()){
					if(!tempcategories.find(category=>category.toLowerCase() === transaction.category)){
					(tempcategories).push((transaction.category).toUpperCase())
					}
				}		
			})}
			
			setCategories(tempcategories)

			let tempTotalExpense = 0
			if(data.transactions !== undefined){data.transactions.forEach(transaction=>{
				if(transaction.type === 'expense'){
				tempTotalExpense += transaction.amount}
			})}
			
			setTotalExpense(tempTotalExpense)

			let tempTotalIncome = 0
			if(data.transactions !== undefined){data.transactions.forEach(transaction=>{
				if(transaction.type === 'income'){
				tempTotalIncome += transaction.amount}
			})}
			
			setTotalIncome(tempTotalIncome)	
		})
		if(user.id === null){
			unsetUser();
	        Swal.fire({
					icon:'info',
					title:'You Have Been Logged Out',
					text: 'Please Login Again',
					footer: 'You will be redirected to Login page.'
				})
	        Router.push('/login')
		}
	},[user.id, allTransactions])


	// functions for recording of transactions
	const [showAddcategoryModal, setShowAddcategoryModal] = useState(false)
	const handleShowAddcategoryModal = () => setShowAddcategoryModal(true)
	const handleHideAddcategoryModal = () => setShowAddcategoryModal(false)				

	const recordTransaction = e => {
  		event.preventDefault()
  		if(description !== "" && amount !== "" && date !== "" && category !== ""){
	  		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/transactions`, {
	  					method: "POST",
	                    headers : {
							'Authorization': `Bearer ${localStorage.getItem('token')}`,
                			'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							category: category.toLowerCase(),
							amount: amount,
							description: description.toLowerCase(),
							date: date,
							type: type.toLowerCase()
						})
	         })
	        .then( res => res.json())
	        .then(data =>{
	        	if(data){
	        		Swal.fire({
                        icon:'success',
                        title:`New ${type} Has Been Added`
                    })
  				setDescription('')
  				setAmount('')
  				setDate('')
  				setCategory('')
  				setCategoryToBeAdded('')
  				setShowAddModal(false)
	        	}
	        })

  		} else {
  			 Swal.fire({
                        icon:'error',
                        title:'Not Processed!',
                        text: 'Incomplete Details',
                        footer: 'Please fill the missing inputs'
                    })
  		}
  	}


 	const [categoryToBeAdded, setCategoryToBeAdded] = useState('')

  	const addingcategory = e => {
  		e.preventDefault()
  		if(!categories.find(category => category.toLowerCase() === categoryToBeAdded.toLowerCase())){
  			Swal.fire({
                    icon:'info',
                    title:'Category Has Been Added!'
                })
  			setCategory(categoryToBeAdded)
  			setShowAddcategoryModal(false)
  		} else {
  			Swal.fire({
                    icon:'error',
                    title:'Category Already Existed!'
                })
  		}
  	}

  	// functions for Modal
  	const handleRecordIncome = () => {
  		setType('Income')
  		setShowAddModal(true)
  	}
  	const handleRecordExpense = () => {	
  		setType('Expense')
  		setShowAddModal(true)
  	}
  	// modals for show
	const [showAddModal, setShowAddModal] = useState(false)	
	const handleShowAddModal = () => setShowAddModal(true) 
	const handleHideAddModal = () => setShowAddModal(false) 

  	

	return(
		<>
				<Row>
					<Col sm={10} md={6}>
						<p className="text-left">Welcome Boss 
							<span className="text-success font-weight-bold"> {customerName.replace(/(^\w|\s\w)/g, m => m.toUpperCase())}
							</span>,
						</p>
						<div className='text-center'>
							<Image src='/service.png' width='300' height='300' alt='logo'/>
							<h2 className="text-success font-weight-bold">
											Php {' '}
											{totalBalance.toFixed(2)}
							</h2>
						</div>
					</Col>
					<Col className="mt-4 text-left btn-group d-flex flex-column">
						<Col>
							<Button onClick={()=>{handleRecordIncome()}} className="btn btn-dark btn-lg btn-block border border-light rounded-0">
								<Row className="d-flex">
									<Col className="border" sm={3}>
										<FontAwesomeIcon icon={faCoins} />
									</Col>
									<Col className="border">
										<span>Add Income</span>
									</Col>
								</Row>
							</Button>
						</Col>
						<Col>
							<Button className="btn btn-dark btn-lg btn-block border border-light rounded-0" onClick={()=>{handleRecordExpense()}}>
								<Row className="d-flex">
									<Col className="border" sm={3}>
										<FontAwesomeIcon icon={faCashRegister} />
									</Col>
									<Col className="border">
										<span>Add Expense</span>
									</Col>
								</Row>
							</Button>
						</Col>
						<Col>
							<Link href="/insights">
								<Button className="btn btn-dark btn-lg btn-block border border-light rounded-0">
									<Row className="d-flex">
										<Col className="border" sm={3}>
											<FontAwesomeIcon icon={faChartLine} />
										</Col>
										<Col className="border">
											<span>Show Insights</span>
										</Col>
									</Row>
								</Button>
							</Link>
						</Col>
						<Col>
							<Link href="/transactions">
								<Button className="btn btn-dark btn-lg btn-block border border-light rounded-0">
									<Row className="d-flex">
										<Col className="border" sm={3}>
											<FontAwesomeIcon icon={faScroll} />
										</Col>
										<Col className="border">
											<span>View Transactions</span>
										</Col>
									</Row>
								</Button>
							</Link>
						</Col>
					</Col>
				</Row>
				
				<Row className="text-left border-top border-dark">
					<Col>
						{
							recentUpdate ?
							<>
							<p>Last Added Transaction :</p>
							<p>{recentUpdate}</p>
							</> : 
							<p className='text-danger'>No Recorded Transactions</p>
						}
					</Col>
					
				</Row>
			

		

			{/*Modal for Add Record*/}
			<Modal 
				show={showAddModal} 
				onHide={handleHideAddModal}
				>
				<Modal.Header>
					<Modal.Title className="infoText" >Add {type} Transaction</Modal.Title>		
				</Modal.Header>
				
				<Modal.Body>
					<Form onSubmit={(e) => recordTransaction(e)}>
						<Form.Group controlId="category">
							<DropdownButton variant="secondary border-bottom" title="Choose a Category" alignRight>
							<Dropdown.Header className="infoText">{type} Category</Dropdown.Header>
							<Dropdown.Divider />
								{	
									categories.map(category =>{
									return(
										<Dropdown.Item 
										key={category}
										required 
										onClick={()=> { setCategory(category)}}>
										{category}
										</Dropdown.Item>
									)
								})
							}
							<Dropdown.Divider />
        					<Dropdown.Item eventKey="4" onClick={()=>{handleShowAddcategoryModal()}}>
        						Add Category
        					</Dropdown.Item>
							</DropdownButton>
							<Form.Control 
								type='text' 
								placeholder={category} 
								value={category} 
								required 
								disabled 
							/>
						</Form.Group>

						<Row>
							<Col>
								<Form.Group controlId="date">
									<Form.Label className="infoText">Date : </Form.Label>
									<Form.Control 
										type='date'
										value={date} 
										onChange={e => setDate(e.target.value)} 
										required 
									/>
								</Form.Group>
							</Col>
							<Col>
								<Form.Group controlId="amount">
									<Form.Label className="infoText">Amount : </Form.Label>
									<Form.Control 
										type='number' 
										placeholder="0.00" 
										value={amount} 
										onChange={e => setAmount(e.target.value)} 
										required 
									/>
								</Form.Group>
							</Col>

						</Row>
					
						<Form.Group controlId="description">
							<Form.Label className="infoText">Description of {type} : </Form.Label>
							<Form.Control type='text' 
								placeholder="Enter Description" 
								value={description} 
								onChange={e => setDescription(e.target.value)} 
								required 
							/>
						</Form.Group>
						<Row>
							<Col>
								<Button sm={6} variant="secondary" type="submit" id="submitBtn">Submit</Button>
							</Col>
							<Col className="text-right">
								<Button  sm={6} variant="danger" id="closeBtn" onClick={()=>setShowAddModal(false)}>Cancel</Button>
							</Col>
						</Row>
					</Form>
				</Modal.Body>
			</Modal>

			{/*Modal for Adding Category*/}
			<Modal 	
				show={showAddcategoryModal} 
				onHide={handleHideAddcategoryModal}
				>
				<Modal.Header>
					<Modal.Title className="infoText" >Add {type} Category</Modal.Title>		
				</Modal.Header>	
				<Modal.Body>
					<Form onSubmit={(e)=>addingcategory(e)}>
						<Form.Group>
							<Form.Label>
								Input to Add Category
							</Form.Label>
							<Form.Control
								type='text'
								value={categoryToBeAdded}
								onChange={e => setCategoryToBeAdded(e.target.value)}
								required
								/>
						</Form.Group>
						<Row>
							<Col>
								<Button variant="warning" type="submit" id="submitBtn">Add</Button>
							</Col>
							<Col className="text-right">
								<Button  sm={6} variant="danger" id="closeBtn" onClick={()=>setShowAddcategoryModal(false)}>Cancel</Button>
							</Col>
						</Row>
					</Form>
				</Modal.Body>
			</Modal>
		</>




		)
}


		