import { useState, useEffect} from 'react';
import { Form, Button, Card, Container, Spinner, Row, Col } from 'react-bootstrap';
import Router from 'next/router';
import Head from 'next/head';
import Swal from 'sweetalert2';
import Image from 'next/image';



export default function register(){
	
	const [email, setEmail] = useState('');
	const [fName, setFName] = useState('');
	const [lName, setLName] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [notLoadingButton, setNotLoadingButton] = useState(true)

 //    const [isActive, setIsActive] = useState(false);

 //    useEffect(()=>{
	// 	if((password1 !== '' && password2 !== '' && email !== '') && (password1 == password2)){
	// 		setIsActive(true)

	// 	}else{
	// 		setIsActive(false)

	// 	}
	// },[password1,password2])
   


   function registerUser(e){
    	e.preventDefault();
    	setNotLoadingButton(false)
    	fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/email-exists`, {
            method: "POST",
            headers : {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email : email
            })
        })
        .then( res => res.json())
        .then( data => {
            // if no duplicates found, register the user
            if(data === false) {

            	fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users`,{
		    		method: 'POST',
		    		headers: {
		    			'Content-Type' : 'application/json'
		    		},
		    		body: JSON.stringify({
		    			email: email.toLowerCase(),
		    			password: password1,
		    			fName: fName.toLowerCase(),
		    			lName: lName.toLowerCase()
		    		})
		    	})
		    	.then(res => res.json())
		    	.then(data =>{
		    		if (data === true) {
		    			Swal.fire({
							icon:'success',
							title:'Thank you for registering',
							text:'Please remember your password',
							footer: 'You will be redirected to Login page.'
						})
						Router.push('/login')
		    		}
		    	})
		  } else {
  				Swal.fire({
					icon:'error',
					title:'Email Already Exist'
				})
				setNotLoadingButton(true)
		  } 
		})
    }

	











	return(
		<>
            <Head>
                <title>Register || Money Mo</title>
            </Head>

            <>
            <Card className="my-5 bg-transparent border-0" >
            	<Row className='d-flex flex-item-center'>
            		<Col className='text-center border-0 margin-0 padding-0' sm={10} md={6}>
            				<h1>Create Account</h1>
                            <Image src='/new.png' width='300' height='300' alt='login' />
                   	</Col>
            		<Col className='border-0 margin-top-4 padding-0'>
            			<Card.Body>
				            <Form onSubmit={ e => registerUser(e)} >				   
				                <Row sm={12}>
				                	<Col className='px-1'>
				                		<Form.Group controlId="fName">
						                    <Form.Label>First Name</Form.Label>
						                    <Form.Control
						                        className="bg-white border-0 rounded-0"
						                        type="text"
						                        placeholder="first name"
						                        value={fName}
						                        onChange={ e => setFName(e.target.value)}
						                        required
						                    />
						                </Form.Group>
				                	</Col>
				                	<Col className='px-1'>
				                		<Form.Group controlId="lName">
						                    <Form.Label>Last Name</Form.Label>
						                    <Form.Control
						                    	className="bg-white border-0 rounded-0"
						                        type="text"
						                        placeholder="last name"
						                        value={lName}
						                        onChange={ e => setLName(e.target.value)}
						                        required
						                    />
						                </Form.Group>
				                	</Col>
				                </Row>
			                	
		                		<Row sm={12}>
		                			<Col className='px-1'>
			                			<Form.Group controlId="userEmail">
						                    <Form.Label>Email Address</Form.Label>
						                    <Form.Control
						                        className="bg-white border-0 rounded-0"
						                        type="email"
						                        placeholder="email address"
						                        value={email}
						                        onChange={ e => setEmail(e.target.value)}
						                        required
						                    />
						                    <Form.Text className="text-muted">
						                        This will be hidden on public
						                    </Form.Text>
						                </Form.Group>
					                </Col>
		                		</Row>
		                	
				               
				                <Row sm={12}>
				                	<Col className='px-1'>
				                		 <Form.Group controlId="password1">
						                    <Form.Label>Password</Form.Label>
						                    <Form.Control
						                        className="bg-white border-0 rounded-0"
						                        placeholder="password"
						                        type="password"
						                        value={password1}
						                        onChange={ e => setPassword1(e.target.value)}
						                        required
						                    />
						                </Form.Group>
				                	</Col>
				                	<Col className='px-1'>
				                		<Form.Group controlId="password2">
						                    <Form.Label>Verify Password</Form.Label>
						                    <Form.Control
						                        className="bg-white border-0 rounded-0"
						                        placeholder="repeat password"
						                        type="password"
						                        value={password2}
						                        onChange={ e => setPassword2(e.target.value)}
						                        required
						                    />
						                </Form.Group>
				                	</Col>
				                </Row>
				               

				                

				                {
				                 	notLoadingButton 
				                 	?   
				                    <Button className='rounded-0' variant="dark" type="submit">Submit</Button> 
				                    :
				                    <Button block disabled variant='transparent'>
		                                    <Spinner size="sm" animation="grow" variant="dark" />
		                                    <Spinner size="sm" animation="grow" variant="dark" />
		                                    <Spinner size="sm" animation="grow" variant="dark" />
		                                     {" "}Please wait... 
		                                </Button>

				                }
				            </Form>
		            	</Card.Body>					
            		</Col>
            	</Row>
            	
            </Card>
            </>  
        </>
		)
}