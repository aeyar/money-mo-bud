import { useContext, useEffect } from 'react';
import UserContext from './../UserContext';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function logout() {
    const {unsetUser} = useContext(UserContext);

    useEffect( () => {
        unsetUser();
        Swal.fire({
					icon:'success',
					title:'You Have Logged Out Successfully',
					footer: 'You will be redirected to Login page.'
				})
        Router.push('/login')
    })

    return null
}