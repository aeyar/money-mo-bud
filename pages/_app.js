import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {UserProvider} from '../UserContext';


function MyApp({ Component, pageProps }) {
  
	const [user,setUser] = useState({
		id: null
	})

	const unsetUser = () => {
		localStorage.clear();
		setUser({
			id: null
		})
	}

	
	return (
	  	<div style={{background:'aliceblue'}}>
		  	<UserProvider value={{user, setUser, unsetUser}}>
				  	<NavBar/>
				  	<Container>
				  		<Component {...pageProps} />
				  	</Container>
				  	<Footer/>
		  	</UserProvider>
	  	</div>
	  	)



}

export default MyApp
