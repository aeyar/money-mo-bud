import UserContext from '../UserContext' ;
import {useContext, useState, useEffect} from 'react'
import moment from 'moment';
import MonthlyBarChart from '../components/MonthlyBarChart'
import CategoryDoughnutChart from '../components/CategoryDoughnutChart'
import SavingsTrendLineChart from '../components/SavingsTrendLineChart'
import colorRandomizer from '../colorRandomizer'
import IncomeAndExpensePieChart from '../components/IncomeAndExpensePieChart'
import {Jumbotron, Row, Col} from 'react-bootstrap'
import Router from 'next/router';
import Swal from 'sweetalert2';
import Image from 'next/image';


export default function insights(){


	const {user,unsetUser} = useContext(UserContext)


	// const totalBalance = (totalIncome - totalExpense)

	const [allTransactions,setAllTransactions] = useState([])
	const [totalIncome, setTotalIncome] = useState(0)
	const [totalExpense, setTotalExpense] = useState(0)
	const [monthlyIncome, setMonthlyIncome] = useState([])
	const [monthlyExpense, setMonthlyExpense] = useState([])
	const [expenseCategories,setExpenseCategories] = useState([])
	const [incomeCategories,setIncomeCategories] = useState([])
	const [totalExpenseByCategories,setTotalExpenseByCategories] = useState([])
	const [totalIncomeByCategories,setTotalIncomeByCategories] = useState([])
	const [expenseTransactions, setExpenseTransactions] = useState([])
	const [incomeTransactions, setIncomeTransactions] = useState([])
	const [expenseColorData,setExpenseColorData] = useState([])
	const [incomeColorData,setIncomeColorData] = useState([])
	const [savingsTrend, setSavingsTrend] = useState([])



	// for rendering of Categories, Total Income, Total Expense
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers:{
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setAllTransactions(data.transactions)

			let tempTotalExpense = 0
			let tempTotalIncome = 0
			let tempExpenseCategories = []
			let tempIncomeCategories = []
			let tempTotalExpenseByCategories = 0
			let tempTotalIncomeByCategories = 0
			let tempExpenseTransactions = []
			let tempIncomeTransactions = []

			data.transactions.forEach(transaction=>{
				if(transaction.type === 'expense'){
					tempTotalExpense += transaction.amount ;
					if(!tempExpenseCategories.find(category => category === transaction.category)){
						tempExpenseCategories.push(transaction.category) 
					}

				}
				
				if(transaction.type === 'income'){
					tempTotalIncome += transaction.amount ;
					if(!tempIncomeCategories.find(category => category === transaction.category)){
						tempIncomeCategories.push(transaction.category)
					}
				}
			
				if(transaction.type === 'expense'){
					tempExpenseTransactions.push(transaction)}

				if(transaction.type === 'income'){
					tempIncomeTransactions.push(transaction)}

			})
			setTotalExpense(tempTotalExpense)
			setTotalIncome(tempTotalIncome)
			
			setExpenseCategories(tempExpenseCategories)
			setIncomeCategories(tempIncomeCategories)

			setExpenseTransactions(tempExpenseTransactions)
			setIncomeTransactions(tempIncomeTransactions)

			setExpenseColorData(tempExpenseCategories.map(()=>`#${colorRandomizer()}`))
			setIncomeColorData(tempIncomeCategories.map(()=>`#${colorRandomizer()}`))

			if(data.transactions.length>0){
				let monthlyExpense = [0,0,0,0,0,0,0,0,0,0,0,0]
				let monthlyIncome = [0,0,0,0,0,0,0,0,0,0,0,0]
				

				data.transactions.forEach(transaction =>{
					const i = moment(transaction.date).month()
					if (transaction.type === 'expense'){
					monthlyExpense[i] += transaction.amount} ;

					if (transaction.type === 'income'){
					monthlyIncome[i] += transaction.amount}
					
				})
			setMonthlyExpense(monthlyExpense)
			setMonthlyIncome(monthlyIncome)
			}
		})
		if(user.id === null){
			unsetUser();
	        Swal.fire({
					icon:'info',
					title:'You Have Been Logged Out',
					text: 'Please Login Again',
					footer: 'You will be redirected to Login page.'
				})
	        Router.push('/login')
		}
	},[user.id])



	useEffect(()=>{
		setTotalExpenseByCategories(expenseCategories.map(category => {
			let total = 0
			expenseTransactions.forEach(transaction=>{
				if(transaction.category === category){
					total = total + transaction.amount
				}
			})
			return total
		}))
		setTotalIncomeByCategories(incomeCategories.map(category => {
			let total = 0
			incomeTransactions.forEach(transaction=>{
				if(transaction.category === category){
					total = total + transaction.amount
				}
			})
			return total
		}))
		setSavingsTrend(monthlyIncome.map((income, index) => income / monthlyExpense[index]))


	},[expenseCategories,incomeCategories,monthlyExpense,monthlyIncome])
		
	const tester = "border border-dark"

	// console.log(savingsTrend)
	// console.log(monthlyIncome)
	// console.log(monthlyExpense)

	return(
	<>
		<Row className='border-bottom border-dark'>
			<Col className="d-flex align-items-center" sm={12}>
				<h1 >INSIGHT ANALYSIS</h1>
			</Col>
			<Col>
				<Image src='/trends.png' width='300' height='300' alt='trends'/>
			</Col>
		</Row>
		<Jumbotron className="bg-light border-top rounded-0">
			<MonthlyBarChart 
				monthlyExpense={monthlyExpense}
				monthlyIncome={monthlyIncome}
				allTransactions={allTransactions}
				/>
		</Jumbotron>

		<Jumbotron className="bg-light border-top rounded-0">
			<IncomeAndExpensePieChart 
				totalExpense={totalExpense} 
				totalIncome={totalIncome}
				/>
		</Jumbotron>

		<Jumbotron className="bg-light border-top rounded-0">
			<CategoryDoughnutChart
				expenseCategories={expenseCategories}
				incomeCategories={incomeCategories}
				totalExpenseByCategories={totalExpenseByCategories}
				totalIncomeByCategories={totalIncomeByCategories}
				expenseColorData={expenseColorData}
				incomeColorData={incomeColorData}
				allTransactions={allTransactions}
				/>
		</Jumbotron>

		<Jumbotron className="bg-light border-top rounded-0">
			<SavingsTrendLineChart 
				savingsTrend={savingsTrend}
				allTransactions={allTransactions}
				/>
		</Jumbotron>
	</>
	)
}