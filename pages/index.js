import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link';
import Image from 'next/image';


export default function Home() {
  return (
    <>
      <Head>
        <title>
          Money Mo Budget Buddy
        </title>
      </Head>
      <div style={{padding:'1em'}}>
        <h2 className="text-center mt-40" >Hello, I'm'</h2>
        <h1 className="text-center mt-40" ><Image src='/logo2.png' width='250' height='100' alt='logo'/></h1>
        <h3 className="text-center mt-40" > Your Budget Tracking Buddy</h3>

        <div className="gridcont-a">
          <Image src='/buddy.png' width='280' height='280' alt='logo'/>
          <h3>Get a quick overview</h3>
              <p>about your total incomes and expenses at a glance and in one place.</p>
          <h3>Understand your financial habits</h3>
              <p>to see where your money goes and come from. </p>
        </div>
        <Link href="/login"><button type="button" className="btn btn-dark">Please Login to Get Started</button></Link>
      </div>
   
    </>
  )
}
